package hello.app;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages={"hello"} )
public class Application {
	

    public static void main(String[] args) {
    	
    	    	    
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        
        System.out.println("Let's inspect the beans provided by Spring Boot:");
        
        String[] beanNames = ctx.getBeanDefinitionNames();
        
        Util util = ctx.getBean(Util.class);
 
        
        System.out.println("SB1 = SB2 es ?:" + util.checkSb());
        
        System.out.println("PB1 = PB2 es ?:" + util.checkPb());
        
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }
 
    }
    

}
	 
@Component
class Util {
	   
    @Autowired
    public  SingletonBean sb1 ;
    
    @Autowired
    public SingletonBean sb2;
    
    @Autowired
    public PrototypeBean pb1 ;
    
    @Autowired
    public  PrototypeBean pb2;
    
   public Util() {
		// TODO Auto-generated constructor stub
	}

	public boolean checkSb(){
	    	return this.sb1.equals(this.sb2);
	    }
    
    public boolean checkPb(){
    	return this.pb1.equals(this.pb2);
    }
  }
    
  

