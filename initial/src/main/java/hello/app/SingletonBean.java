package hello.app;

public class SingletonBean {

	private String beanName;
	
	public SingletonBean(){
		
	}
	
	public String getBeanName(){
		return this.beanName;
	}
	
	public void setBeanName(String beanName){
		this.beanName = beanName;
	}
}