package hello.app;

public class PrototypeBean {
	
	private String helloProto;
	
	public PrototypeBean(){
		
	}

	public String getHelloProto() {
		return helloProto;
	}

	public void setHelloProto(String helloProto) {
		this.helloProto = helloProto;
	}

}