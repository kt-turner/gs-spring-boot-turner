package hello.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import hello.app.PrototypeBean;
import hello.app.SingletonBean;

@Configuration
public class AppConfig {
	
	@Bean
	@Scope(value="singleton")
	SingletonBean singletonBean(){
		return new SingletonBean();
	}

	@Bean(name="protoBean")
	@Scope(value="prototype")
	PrototypeBean prototypeBean(){
		return new PrototypeBean();
	}
}

